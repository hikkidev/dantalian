import os
import time
import csv
import argparse
import pickle
import shutil
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.optimizers import Adam
from keras.models import load_model

import util
import math
import config as cfg
import system as sys
import prepare_data as pp
from generator import Generator


def create_tables(type,
                  epoch_size,
                  workspace_dir,
                  speech_dir,
                  noise_dir,
                  snr,
                  factor=1):
    print("\nПодготовка таблиц")
    pp.create_table_csv({
        'workspace': workspace_dir,
        'speech_dir': speech_dir,
        'noise_dir': noise_dir,
        'data_type': type,
        'snr': snr,
        'factor': factor,
        'epoch_size': epoch_size
    })


def augment_data(type, workspace_dir, speech_dir, noise_dir, snr):
    print("\nАугментация данных")
    pp.augment_data({
        'workspace': workspace_dir,
        'speech_dir': speech_dir,
        'noise_dir': noise_dir,
        'data_type': type,
        'snr': snr
    })


def feature_extraction(type, workspace_dir, speech_dir, noise_dir, snr):
    print("\nИзвлечение спектров")
    pp.export_spectrograms({
        'workspace': workspace_dir,
        'speech_dir': speech_dir,
        'noise_dir': noise_dir,
        'data_type': type,
        'snr': snr
    })


def feature_transformations(type, workspace_dir, snr, n_combo, shift):
    print("\nСоздание тензоров")
    pp.conver_to_tensor({
        'workspace': workspace_dir,
        'data_type': type,
        'snr': snr,
        'n_combo': n_combo,
        'shift': shift
    })

    if (type == 'train'):
        pp.compute_scaler({
            'workspace': workspace_dir,
            'data_type': type,
            'snr': snr
        })


def calculate_pesq(args):
    workspace = args['workspace']
    speech_dir = args['speech_dir']
    data_type = args['data_type']
    snr = args['snr']

    fs = cfg.sample_rate

    if os.path.isfile('_pesq_itu_results.csv'):
        os.system('rm _pesq_itu_results.csv')

    if os.path.isfile('_pesq_results.csv'):
        os.system('rm _pesq_results.csv')

    enh_speech_dir = os.path.join(workspace, "enh_audio", data_type,
                                  "%ddb" % int(snr))
    wav_list = os.listdir(enh_speech_dir)
    wav_list.sort()
    for wav_name in wav_list:
        enh_path = os.path.join(enh_speech_dir, wav_name)

        speech_na = wav_name.split('.')[1]
        speech_path = os.path.join(speech_dir, "%s.wav" % speech_na)

        cmd = ' '.join(["./pesq/pesq", speech_path, enh_path, ("+%s" % fs)])
        os.system(cmd)
    get_pesq_stats()


def get_pesq_stats():
    """Рассчитать статистику PESQ

    """
    pesq_path = "_pesq_results.csv"
    with open(pesq_path) as f:
        reader = csv.reader(f, delimiter='\t')
        lis = list(reader)

    pesq_res = {}
    for i1 in range(1, len(lis) - 1):
        li = lis[i1]
        na = li[0]
        pesq_value = float(li[1])
        noise_name = na.split('.')[1]
        if noise_name not in pesq_res.keys():
            pesq_res[noise_name] = [pesq_value]
        else:
            pesq_res[noise_name].append(pesq_value)

    avg_list, std_list = [], []
    f = "{0:<16} {1:<16}"
    print(f.format("Шум", "PESQ_MOS"))
    print("---------------------------------")
    for noise_name in pesq_res.keys():
        pesqs = pesq_res[noise_name]
        avg_pesq = np.mean(pesqs)
        std_pesq = np.std(pesqs)
        avg_list.append(avg_pesq)
        std_list.append(std_pesq)
        print(f.format(noise_name, "%.2f +- %.2f" % (avg_pesq, std_pesq)))
    print("---------------------------------")
    print(
        f.format("Среднее",
                 "%.2f +- %.2f" % (np.mean(avg_list), np.mean(std_list))))


def validate_model(model, gen, x, y):
    est_y_all, y_all = [], []

    for (batch_x, batch_y) in gen.generate(xs=[x], ys=[y]):
        pred = model.predict(batch_x)
        est_y_all.append(pred)
        y_all.append(batch_y)

    est_y_all = np.concatenate(est_y_all, axis=0)
    y_all = np.concatenate(y_all, axis=0)

    loss = util.mean_absolute_error(y_all, est_y_all)
    return loss


def build_model(n_hid, n_combo, n_freq):
    model = Sequential()
    model.add(Flatten(input_shape=(n_combo, n_freq)))
    model.add(Dense(n_hid, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(n_hid, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(n_hid, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(n_freq, activation='linear'))
    model.summary()
    return model


def save_train_stats(iter, train_loss, model_loss, stats_dir):
    print("Итерация: %d, train_loss: %f, model_loss: %f" %
          (iter, train_loss, model_loss))
    stat_path = os.path.join(stats_dir, "%diter.p" % iter)
    pickle.dump(
        {
            'iter': iter,
            'train_loss': train_loss,
            'model_loss': model_loss,
        },
        open(stat_path, 'wb'),
        protocol=pickle.HIGHEST_PROTOCOL)


def train_dnn(args):
    timestamp = time.time()
    workspace = args['workspace']
    snr_train = args['snr_train']
    learning_rate = args['learning_rate'],
    self_clean = args['self_clean']

    print("\nТренировка нейросети")

    # Общее количество обучающих примеров, представленных в одной партии.
    batch_size = 500
    # Стадия подготовки данных

    train_path = os.path.join(workspace, "tensor", "train",
                              "%ddb" % int(snr_train), "data.h5")
    (tr_x, tr_y) = sys.load_hdf5(train_path)
    print("Загрузка данных завершена.\t%s sec" % (time.time() - timestamp))

    print("До завершения эпохи потребуется итераций: %d" %
          int(tr_x.shape[0] / batch_size))

    timestamp = time.time()
    scaler_path = os.path.join(workspace, "tensor", "train",
                               "%ddb" % int(snr_train), "scaler.p")
    scaler = pickle.load(open(scaler_path, 'rb'))
    tr_x = util.scale_on_3d(tr_x, scaler)
    tr_y = util.scale_on_2d(tr_y, scaler)
    print("Нормализация scaler-ом завершена.\t%s sec" %
          (time.time() - timestamp))

    timestamp = time.time()
    (_, n_combo, n_freq) = tr_x.shape
    n_hid = 2048
    if (args['model_path'] is None):
        model = build_model(n_hid, n_combo, n_freq)
        model.compile(loss='mean_absolute_error',
                      optimizer=Adam(lr=learning_rate))
        print("Создание модели завершено.\t%s sec" % (time.time() - timestamp))
    else:
        model = load_model(args['model_path'])
        print("Загрузка модели завершена.\t%s sec" % (time.time() - timestamp))

    train_generator = Generator(batch_size=batch_size, type='train')
    test_generator = Generator(batch_size=batch_size,
                               type='test',
                               test_iters=100)

    model_dir = os.path.join(workspace, "model", "%ddb" % int(snr_train))
    stats_dir = os.path.join(workspace, "training_stats",
                             "%ddb" % int(snr_train))

    sys.create_folder(model_dir)
    sys.create_folder(stats_dir)

    iter = 0
    train_loss = validate_model(model, test_generator, tr_x, tr_y)
    save_train_stats(iter, train_loss, -1, stats_dir)

    # Стадия обучения
    timestamp = time.time()
    for (batch_x, batch_y) in train_generator.generate(xs=[tr_x], ys=[tr_y]):
        model_loss = model.train_on_batch(batch_x, batch_y)
        iter += 1

        if iter % 1000 == 0:
            train_loss = validate_model(model, test_generator, tr_x, tr_y)
            save_train_stats(iter, train_loss, model_loss, stats_dir)

        if iter % 5000 == 0:
            model_path = os.path.join(model_dir, "md_%diter.h5" % iter)
            model.save(model_path)

        if iter == 10001:
            break
    print("Обучение завершено.\t%s sec" % (time.time() - timestamp))
    if (self_clean):
        os.remove(
            os.path.join(workspace, "tensor", "train", "%ddb" % int(snr_train),
                         "data.h5"))
        shutil.rmtree(
            os.path.join(workspace, "spectrogram", "train",
                         "%ddb" % int(snr_train)))


def validate_dnn(args):
    n_combo = args['n_combo']
    workspace = args['workspace']
    snr_test = args['snr_test']
    snr_train = args['snr_train']
    model_path = args['model']
    self_clean = args['self_clean']
    frame_step = cfg.overlap()
    fs = cfg.sample_rate

    print("\nТестирование нейросети")
    print("SNR_test = %d", snr_test)

    model = load_model(model_path)

    scaler_path = os.path.join(workspace, "tensor", "train",
                               "%ddb" % int(snr_train), "scaler.p")
    scaler = pickle.load(open(scaler_path, 'rb'))

    feat_dir = os.path.join(workspace, "spectrogram", "test",
                            "%ddb" % int(snr_test))
    names = os.listdir(feat_dir)
    len_feat = len(names)
    timestamp = time.time()
    for (count, na) in enumerate(names):
        n_pad = int((n_combo - 1) / 2)
        feat_path = os.path.join(feat_dir, na)
        [_, mixed_original_spectrum] = pickle.load(open(feat_path, 'rb'))

        mixed_spectrum = np.abs(mixed_original_spectrum.T)
        mixed_spectrum = util.pad_with_border(mixed_spectrum, n_pad)
        mixed_spectrum = math.safe_log(mixed_spectrum)
        mixed_spectrum = util.scale_on_2d(mixed_spectrum, scaler)

        mixed_tensor = util.mat_2d_to_3d(mixed_spectrum, n_combo, shift=1)
        mixed_spectrum = model.predict(mixed_tensor)

        mixed_spectrum = util.inverse_scale_on_2d(mixed_spectrum, scaler)
        mixed_spectrum = np.exp(mixed_spectrum)
        mixed_spectrum = math.real_to_complex(mixed_spectrum.T,
                                              mixed_original_spectrum)

        restored_signal = math.istft(mixed_spectrum, frame_step)

        out_path = os.path.join(workspace, "enh_audio", "test",
                                "%ddb" % int(snr_test),
                                "enh.%s.wav" % na.replace('.p', ''))
        sys.create_folder(os.path.dirname(out_path))
        sys.write_audio(out_path, restored_signal, fs)
        if (count % 100 == 0):
            print("Осталось записей: %d" % int(len_feat - count))
    if (self_clean):
        shutil.rmtree(
            os.path.join(workspace, "tensor", "test", "%ddb" % int(snr_test)))
        shutil.rmtree(
            os.path.join(workspace, "spectrogram", "test",
                         "%ddb" % int(snr_test)))
    print("Всего %d записей очищено от шума.\t%s sec" %
          (len_feat, time.time() - timestamp))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='mode')

    parser_1 = subparsers.add_parser('train')
    parser_1.add_argument('--workspace', type=str, required=True)
    parser_1.add_argument('--speech_dir', type=str, required=True)
    parser_1.add_argument('--noise_dir', type=str, required=True)
    parser_1.add_argument('--snr', type=float, required=True)
    parser_1.add_argument('--factor', type=int, required=True)
    parser_1.add_argument('--lr', type=float, required=False, default=0.0001)
    parser_1.add_argument('--model', type=str, required=False, default=None)
    parser_1.add_argument('--n_combo', type=int, required=False, default=7)

    parser_2 = subparsers.add_parser('validate')
    parser_2.add_argument('--workspace', type=str, required=True)
    parser_2.add_argument('--speech_dir', type=str, required=True)
    parser_2.add_argument('--noise_dir', type=str, required=True)
    parser_2.add_argument('--snr_test', type=float, required=True)
    parser_2.add_argument('--snr_train', type=float, required=True)
    parser_2.add_argument('--model', type=str, required=True)
    parser_2.add_argument('--n_combo', type=int, required=False, default=7)

    args = parser.parse_args()

    if args.mode == 'train':
        create_tables('train', args.workspace, args.speech_dir,
                      args.noise_dir, args.snr, args.factor)
        augment_data('train', args.workspace, args.speech_dir,
                     args.noise_dir, args.snr)
        feature_extraction('train', args.workspace, args.speech_dir,
                           args.noise_dir, args.snr)
        feature_transformations('train', args.workspace, args.snr,
                                args.n_combo, 3)
        train_dnn({
            'workspace': args.workspace,
            'snr_train': args.snr,
            'learning_rate': args.lr,
            'self_clean': True,
            'model_path': args.model
        })

    elif args.mode == 'test':
        create_tables('test', args.workspace, args.speech_dir,
                      args.noise_dir, args.snr_test, 1)
        augment_data('test', args.workspace, args.speech_dir,
                     args.noise_dir, args.snr_test)
        feature_extraction('test', args.workspace, args.speech_dir,
                           args.noise_dir, args.snr_test)
        feature_transformations('test', args.workspace, args.snr_test,
                                args.n_combo, 3)
        validate_dnn({
            'workspace': args.workspace,
            'snr_train': args.snr_train,
            'snr_test': args.snr_test,
            'n_combo': args.n_combo,
            'self_clean': True,
            'model_path': args.model
        })

        calculate_pesq({
            'workspace': args.workspace,
            'speech_dir': args.speech_dir,
            'data_type': 'test',
            'snr': args.snr_test
        })

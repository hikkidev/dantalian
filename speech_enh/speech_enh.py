
import numpy as np
import pickle
import argparse
from keras.models import load_model

import math
import util
import system as sys
import config as cfg


def decision_directed(spectrum, start_frame, alpha=0.98):
    """ Вычисление функции спектральной коррекции
    Args:
        spectrum: 2darray - амплитудный спектр
        frame_length: int - количество звуковых отсчётов в одном кадре
        alpha: float - cглаживающий  коэффициент
    Outputs:
        gains: 1darray - коэффициенты усиления для каждого кадра
        noise_estimation: 1darray - оценка шума

    """
    # Оценка шума по первым N кадрам
    noise_est = np.mean(np.abs(spectrum[:, :start_frame - 1]), axis=1)

    g = np.ones(noise_est.shape)
    previous_prior_snr = np.zeros(g.shape)
    num_frames = spectrum.shape[1]
    gains = []

    for frame_number in range(num_frames):
        frame = np.abs(spectrum[:, frame_number])

        # апостериорное ОСШ
        snr_post = np.divide(np.square(frame), noise_est)

        # оценка априорного ОСШ
        est_snr_prior = (alpha * previous_prior_snr) + (1 - alpha) * snr_post

        # спектральная коррекция (коэффициент  усиления) для текущего кадра
        g = np.divide(est_snr_prior, 1 + est_snr_prior)
        gains.append(g)

        previous_prior_snr = est_snr_prior.copy()

    return np.asarray(gains).T, noise_est


def tsnr(spectrum, dd_gains, noise_est):
    """ Уменьшает негативные эффекты от decision_directed (DD)
    Args:
        spectrum: 2darray - амплитудный спектр
        dd_gains: 1darray - коэффициенты  усиления для каждого кадра
        noise_est: 1darray - оценка шума
    Outputs:
        gains: 1darray - коэффициенты усиления для каждого кадра
        est_spectr: 2darray [shape=(1 + frame_step/2, n_frames)] - оценка
            амплитудного спектра речи

    """
    num_frames = spectrum.shape[1]
    spectr_est = np.zeros(spectrum.shape, dtype=np.complex)
    gains = []

    for frame_number in range(num_frames):
        frame = np.abs(spectrum[:, frame_number])

        # оценка априорного ОСШ
        est_snr_prior = ((dd_gains[:, frame_number] * frame)**2) / noise_est

        # коэффициент усиления
        g = np.divide(est_snr_prior, 1 + est_snr_prior)
        gains.append(g)

        # оценка амплитудного спектра речи для текущего кадра
        spectr_est[:, frame_number] = g * spectrum[:, frame_number]

    return np.asarray(gains).T, spectr_est


def hrnr(spectrum, spectr_est, tsnr_gains, noise_est, NL="max"):
    """ Восстанавливает гармоники потерянные на этапе TSNR
    Args:
        spectrum: 2darray - амплитудный спектр
        spectr_est: 2darray - оценка амплитудного спектра по tsnr
        tsnr_gains: 1darray - коэффициенты усиления для каждого кадра
        noise_est: 1darray - оценка шума
        NL: str - нелинейная функция
    Outputs:
        out_spectrum: 2darray [shape=(1 + frame_step/2, n_frames)] - оценка
            амплитудного спектра речи

    """

    # применяется нелинейная функция NL
    # к восстановленному по TSNR-методу речевому сигналу
    harmo_spectrum = spectr_est.copy()
    if NL == "abs":
        harmo_spectrum = np.abs(harmo_spectrum)
    else:
        harmo_spectrum[harmo_spectrum <= 0] = 0.01

    num_frames = spectr_est.shape[1]
    out_spectrum = np.zeros(spectr_est.shape, dtype=np.complex)

    for frame_number in range(num_frames):
        est_frame = np.abs(spectr_est[:, frame_number])
        harmo_frame = np.abs(harmo_spectrum[:, frame_number])
        gain_tsnr = tsnr_gains[:, frame_number]

        # оценка априорного ОСШ
        A = gain_tsnr * (np.abs(est_frame)**2)
        B = (1 - gain_tsnr) * (np.abs(harmo_frame)**2)
        est_snr_prior = (A + B) / noise_est

        # коэффициент  усиления
        g = np.divide(est_snr_prior, 1 + est_snr_prior)

        # оценка амплитудного спектра речи для текущего кадра
        out_spectrum[:, frame_number] = g * spectrum[:, frame_number]

    return out_spectrum


def afw_filter(signal, sample_rate, frame_length, frame_step):
    """Адаптивный фильтр

    Args:
        signal: 1darray - аудио поток
        samplerate: int, частота дискретизации
        frame_length: int - количество звуковых отсчётов в одном кадре
        frame_step: int - количество звуковых отсчётов между
        началами следующих друг за другом кадров
    Outputs:
        spectrogram: 2darray [shape=(1 + frame_length / 2, n_frames)]

    """
    spectrum = math.stft(signal, frame_length, frame_step)
    (dd_gains, noise_est) = decision_directed(spectrum, 13)
    (tsnr_gains, tsnr_est_spectrum) = tsnr(spectrum, dd_gains, noise_est)
    hrnr_est_spectrum = hrnr(spectrum, tsnr_est_spectrum, tsnr_gains,
                             noise_est)
    return hrnr_est_spectrum


def dnn_filter(signal, sample_rate, frame_length, frame_step, n_combo, model):
    n_pad = int((n_combo - 1) / 2)

    mixed_spectrum = math.stft(mixed_audio, frame_length, frame_step)

    spectrum = np.abs(mixed_spectrum.T)
    spectrum = util.pad_with_border(spectrum, n_pad)
    spectrum = math.safe_log(spectrum)

    mixed_tensor = util.mat_2d_to_3d(spectrum, n_combo, shift=1)

    spectrum = model.predict(mixed_tensor)
    mixed_tensor = None

    spectrum = np.exp(spectrum)
    spectrum = math.real_to_complex(spectrum.T, mixed_spectrum)
    return spectrum


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='mode')

    p_enh = subparsers.add_parser('dnn')
    p_enh.add_argument('--audio_dir', type=str, required=True)
    p_enh.add_argument('--out_dir', type=str, required=True)
    p_enh.add_argument('--fs', type=int, required=True)
    p_enh.add_argument('--model', type=str, required=True)

    p_wfiler = subparsers.add_parser('afw')
    p_wfiler.add_argument('--audio_dir', type=str, required=True)
    p_wfiler.add_argument('--out_dir', type=str, required=True)
    p_wfiler.add_argument('--fs', type=str, required=True)

    args = parser.parse_args()

    n_combo = cfg.n_combo
    frame_length = cfg.frameSize()
    frame_step = cfg.overlap()

    (mixed_audio, _) = sys.read_audio(args.audio_dir, args.fs)

    if args.mode == 'dnn':
        model = load_model(args.model)
        spectrum = dnn_filter(mixed_audio, args.fs, frame_length, frame_step,
                              n_combo, model)
        restored_signal = math.istft(spectrum, frame_step)
        sys.write_audio(args.out_dir, restored_signal, args.fs)
    elif args.mode == 'afw':
        spectrum = afw_filter(mixed_audio, args.fs, frame_length, frame_step)
        restored_signal = math.istft(spectrum, frame_step)
        sys.write_audio(args.out, restored_signal, args.fs)

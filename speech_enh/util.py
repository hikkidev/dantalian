# -*- coding: utf-8 -*-
import numpy as np


def mat_2d_to_3d(x, agg_num, shift):
    len_x, n = x.shape
    if (len_x < agg_num):
        x = np.concatenate((x, np.zeros((agg_num - len_x, n))))

    len_x = len(x)
    start = 0
    result = []
    while (start + agg_num <= len_x):
        result.append(x[start:start + agg_num])
        start += shift
    return np.array(result)


def pad_with_border(x, n_pad):
    x_pad_list = [x[0:1]] * n_pad + [x] + [x[-1:]] * n_pad
    return np.concatenate(x_pad_list, axis=0)


def rms(signal):
    return np.sqrt(np.mean(np.abs(signal)**2, axis=0, keepdims=False))


def mean_absolute_error(y, est_y):
    return np.mean(np.abs(est_y - y))


def scale_on_2d(x, scaler):
    return scaler.transform(x)


def scale_on_3d(x, scaler):
    (n_segs, n_concat, n_freq) = x.shape
    result = x.reshape((n_segs * n_concat, n_freq))
    result = scaler.transform(result)
    x = result.reshape((n_segs, n_concat, n_freq))
    return x


def inverse_scale_on_2d(x, scaler):
    return x * scaler.scale_[None, :] + scaler.mean_[None, :]

# -*- coding: utf-8 -*-
import numpy as np
import librosa


MAX_BLOCK_SIZE = 2**8 * 2**10  # 256 КБ


def _normalize(s, window, n_frames, frame_length, frame_step):
    ifft_window_sum = librosa.filters.window_sumsquare(window.reshape(-1),
                                                       n_frames,
                                                       n_fft=frame_length,
                                                       hop_length=frame_step)
    if np.issubdtype(ifft_window_sum.dtype, np.floating) or \
       np.issubdtype(ifft_window_sum.dtype, np.complexfloating):
        tiny = np.finfo(ifft_window_sum.dtype).tiny
    else:
        tiny = np.finfo(np.float32).tiny

    indxs = ifft_window_sum > tiny
    s[indxs] /= ifft_window_sum[indxs]
    s = s[int(frame_length // 2):-int(frame_length // 2)]
    return s


def calculate_frames(signal, frame_length, frame_step):
    """Разбить аудио сигнал на N кадров

    Args:
        signal: 1darray - аудио поток
        frame_length: int - количество звуковых отсчётов в одном кадре
        frame_step: int - количество звуковых отсчётов между
            началами следующих друг за другом кадров
    Outputs:
        out: 2darray, [shape=(n_frames, 1 + frame_step/2)]

    """
    n_frames = 1 + (signal.shape[-1] - frame_length) // frame_step
    strides = np.asarray(signal.strides)

    new_stride = np.prod(
        strides[strides > 0] // signal.itemsize) * signal.itemsize
    shape = list(signal.shape)[:-1] + [frame_length, n_frames]
    strides = list(strides) + [frame_step * new_stride]
    return np.lib.stride_tricks.as_strided(signal,
                                           shape=shape,
                                           strides=strides)


def hamming_window(frame_length):
    """Вычисление окна Хэмминга

    Args:
        frame_length: int - количество звуковых отсчётов в одном кадре
    Outputs:
        window: 1darray

    """
    if frame_length < 1:
        return np.array([])
    if frame_length == 1:
        return np.ones(1, float)
    n = np.arange(0, frame_length)
    return 0.54 - 0.46 * np.cos(2.0 * np.pi * n / (frame_length - 1))


def stft(signal, frame_length, frame_step):
    """ Представляет сигнал в частотно-временной области через БПФ
        применяя оконную функцию Хемминга для каждого кадра.

    Args:
        signal: 1darray - аудио поток
        frame_length: int - количество звуковых отсчётов в одном кадре
        frame_step: int - количество звуковых отсчётов между
            началами следующих друг за другом кадров
    Outputs:
        out: 2darray, [shape=(1 + frame_length/2, n_frames)] -
            данные stft преобразования

    """
    window = hamming_window(frame_length).reshape((-1, 1))

    # Центрировать сигнал, чтобы получить правильное кол-во кадров
    signal = np.pad(signal, int(frame_length // 2), mode='reflect')

    frames = calculate_frames(signal, frame_length, frame_step)

    out = np.empty((int(1 + frame_length // 2), frames.shape[1]),
                   dtype=np.complex,
                   order='F')
    # Кэш оптимизация
    n_columns = int(MAX_BLOCK_SIZE / (out.shape[0] * out.itemsize))

    for b_start in range(0, out.shape[1], n_columns):
        b_end = min(b_start + n_columns, out.shape[1])
        block = window * frames[:, b_start:b_end]
        out[:, b_start:b_end] = np.fft.rfft(block, axis=0)
    return out


def istft(spectrum, frame_step):
    """ Обратное преобразование Фурье

    Args:
        frames: 2darray - stft данные
        frame_step: int - количество звуковых отсчётов между
            началами следующих друг за другом кадров
    Outputs:
        signal: 1darray - массив амплитуд (сигнал)

    """
    n_frames = spectrum.shape[1]
    frame_length = 2 * (spectrum.shape[0] - 1)
    signal_len = frame_length + frame_step * (n_frames - 1)

    signal = np.zeros(signal_len, dtype=np.float64)
    window = hamming_window(frame_length)[:, np.newaxis]

    # Кэш оптимизация
    n_columns = int(MAX_BLOCK_SIZE //
                    (spectrum.shape[0] * spectrum.itemsize))

    processed_frames = 0
    for b_start in range(0, n_frames, n_columns):
        b_end = min(b_start + n_columns, n_frames)

        block = window * np.fft.irfft(spectrum[:, b_start:b_end], axis=0)
        shift = block.shape[0]

        signal_part = signal[processed_frames * frame_step:]
        # Наложение - добавить блок istft, для конкретного кадра
        for frame_number in range(block.shape[1]):
            s_start = frame_number * frame_step
            signal_part[s_start:(s_start + shift)] += block[:, frame_number]
        processed_frames += (b_end - b_start)

    return _normalize(signal, window, n_frames, frame_length, frame_step)


def safe_log(x):
    return np.log(x + 1e-08)


def real_to_complex(abs_spectrum, original_spectrum):
    """Восстанавливает фазу спектрограммы на основе фазы оригинального спекта

    """
    theta = np.angle(original_spectrum)
    return abs_spectrum * np.exp(1j * theta)

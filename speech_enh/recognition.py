import os
from pocketsphinx import Pocketsphinx
from pocketsphinx import Decoder
import wave

model_path = "/home/sora/documents/nstu/nstu-ai/voxforge_ru/"
data_path = "/home/sora/documents/nstu/source_8kHz_16bit/dataset_mozilla_speech/validated/"

SAMPLE_RATE = 8000
CHUNK_SIZE = 1024
wav_file = os.path.join(data_path, '0a0aae6af1ba25bb7468b8a30c8ecdc2.wav')

# Create a decoder with certain model
config = Decoder.default_config()
config.set_string(
    '-hmm',
    os.path.join(model_path, 'model_parameters', 'voxforge_ru.cd_cont_3000'))
config.set_string('-lm', os.path.join(model_path, 'etc', 'voxforge_ru.lm.bin'))
config.set_string('-dict', os.path.join(model_path, 'etc', 'voxforge_ru.dic'))

# Creaders decoder object for streaming data.
decoder = Decoder(config)

decoder.start_utt()
stream = open(wav_file, 'rb')
while True:
    buf = stream.read(1024)
    if buf:
        decoder.process_raw(buf, False, False)
    else:
        break
decoder.end_utt()

hypothesis = decoder.hyp()
logmath = decoder.get_logmath()
print('Best hypothesis: ', hypothesis.hypstr, " model score: ",
      hypothesis.best_score, " confidence: ", logmath.exp(hypothesis.prob))

print('Best hypothesis segments: ', [seg.word for seg in decoder.seg()])

# Access N best decodings.
print('Best 10 hypothesis: ')
for best, i in zip(decoder.nbest(), range(10)):
    print(best.hypstr, best.score)

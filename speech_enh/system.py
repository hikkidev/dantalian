# -*- coding: utf-8 -*-
import os
import soundfile
import librosa
import numpy as np
import h5py


def create_folder(folder):
    """Создать папку, если её не существует

    Args:
        folder: str, путь к папке

    """
    if not os.path.exists(folder):
        os.makedirs(folder)


def read_audio(path, target_srate=None):
    """Считать аудио файл

    Args:
        path: str, путь к аудио файлу
        target_srate: int, частота дискретизации
    Outputs:
        audio: 1darray - массив амплитуд
        fs: int - частота дискретизации

    """
    (audio, fs) = soundfile.read(path)
    if audio.ndim > 1:
        audio = np.mean(audio, axis=1)
    if target_srate is not None and fs != target_srate:
        audio = librosa.resample(audio, orig_sr=fs, target_sr=target_srate)
        fs = target_srate
    return audio, fs


def write_audio(path, signal, fs):
    """Сохранить аудио файл

    Args:
        path: str, путь к аудио файлу
        signal: (numpy.ndarray) - аудио поток
        fs: int, частота дискретизации

    """
    soundfile.write(file=path, data=signal, samplerate=fs)


def load_hdf5(h5file_path):
    with h5py.File(h5file_path, 'r') as h5file:
        x = np.array(h5file.get('x'))
        y = np.array(h5file.get('y'))
    return x, y

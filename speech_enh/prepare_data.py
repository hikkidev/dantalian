#!usr/bin/python
# -*- coding: utf-8 -*-
import os
import numpy as np
import csv
import pickle
import h5py
import time
from sklearn import preprocessing

import math
import util
import config as cfg
import system as sys


def get_amplitude_scaling_factor(signal, noise, snr):
    """Вычислить коэффициент масштабирования амплитуды речи

    Args:
        signal: 1darray - аудио поток речи
        noise: 1darray - аудио поток шума
        snr: float, отношение мощности полезного сигнала к мощности шума (дБ)
            SNR = 20*Log(RMS(Signal Voltage) / RMS(Noise Voltage))
    Outputs:
        float, коэффициент масштабирования

    """
    original_ratio = util.rms(signal) / util.rms(noise)
    target_ratio = 10.0**(float(snr) / 20.0)
    return target_ratio / original_ratio


def superimpose_and_normalize(signal, noise):
    """Наложить шум на речь и нормализовать

    Args:
        signal: 1darray - аудио поток речи
        noise: 1darray - аудио поток шума
    Outputs:
        signal: 1darray - нормализованный аудио поток речи
        noise: 1darray - нормализованный аудио поток шума
        combine: 1darray - нормализованный аудио поток речи и шума
        factor: float - коэффициент нормализации

    """
    # объединяем
    combine = signal + noise

    # нормализуем
    factor = 1.0 / np.max(np.abs(combine))
    combine *= factor
    signal *= factor
    noise *= factor
    return signal, noise, combine, factor


def noise_fit(clean_audio, noise_audio, noise_onset, noise_offset):
    """Наложить шум на чистую речь

    Args:
      clean_audio: 1darray, аудио поток
      noise_audio: 1darray, аудио поток
      noise_onset: int, метка начала шума
      noise_offset: int, метка конеца шума

    """
    if clean_audio.size < noise_audio.size:
        # Усечь шум до той же длины, что и речь
        return noise_audio[noise_onset:noise_offset]
    else:
        # Повторять шум до той же длины, что и речь
        ratio = float(clean_audio.size) / float(noise_audio.size)
        n_repeat = int(np.ceil(ratio))
        noise_audio_ex = np.tile(noise_audio, n_repeat)
        return noise_audio_ex[0:clean_audio.size]


def create_table_csv(args):
    """Создать CSV файл, содержащий информацию для аугментации
    Формат файла: [speech_name, noise_name, noise_onset, noise_offset]

    Args:
      workspace: str, путь к рабочему пространству
      speech_dir: str, путь к речевым данным
      noise_dir: str, путь к шумам
      data_type: str, тип данных для работы - ['train' | 'test']
      factor: int, множитель шумов используется в тренировке
      snr: float, отношение сигнал/шум

    """
    timestamp = time.time()
    workspace = args['workspace']
    speech_dir = args['speech_dir']
    noise_dir = args['noise_dir']
    data_type = args['data_type']
    factor = args['factor']
    snr = args['snr']

    rs = np.random.RandomState(0)
    noise_names = []
    speech_names = []

    for names in os.listdir(speech_dir):
        if names.lower().endswith(".wav"):
            speech_names.append(names)

    for names in os.listdir(noise_dir):
        if names.lower().endswith(".wav"):
            noise_names.append(names)

    csv_path = os.path.join(workspace, "tables",
                            "%ddb" % int(snr), "%s.csv" % data_type)
    sys.create_folder(os.path.dirname(csv_path))

    header = "speech_name\tnoise_name\tnoise_onset\tnoise_offset\n"
    f = open(csv_path, 'w')
    f.write(header)
    for speech_name in speech_names:
        speech_path = os.path.join(speech_dir, speech_name)
        (speech_audio, _) = sys.read_audio(speech_path)
        len_speech = speech_audio.size
        # Для тренировочных данных смешайте каждую речь
        # со случайным образом выбранным шумом
        if data_type == 'train':
            selected_noise_names = \
             rs.choice(noise_names, size=factor, replace=False)
        else:
            selected_noise_names = noise_names

        # Смешать одну речь с разными шумами много раз
        for noise_name in selected_noise_names:
            noise_path = os.path.join(noise_dir, noise_name)
            (noise_audio, _) = sys.read_audio(noise_path)
            len_noise = noise_audio.size

            # Если шум длиннее речи, то выбрать случайный сегмент шума
            if len_noise > len_speech:
                noise_onset = rs.randint(0, len_noise - len_speech, size=1)[0]
                noise_offset = noise_onset + len_speech
            else:
                noise_onset = 0
                noise_offset = len_speech

            f.write("%s\t%s\t%d\t%d\n" %
                    (speech_name, noise_name, noise_onset, noise_offset))
    f.close()
    print("Создание таблицы %s.csv завершено.\t%s sec" %
          (data_type, (time.time() - timestamp, )))


def augment_data(args):
    """
    Args:
      workspace: str, путь к рабочему пространству
      speech_dir: str, путь к речевым данным
      noise_dir: str, путь к шумам
      data_type: str, тип данных для работы - ['train' | 'test']
      snr: float, отношение сигнал/шум

    """
    timestamp = time.time()
    workspace = args['workspace']
    speech_dir = args['speech_dir']
    noise_dir = args['noise_dir']
    data_type = args['data_type']
    snr = args['snr']
    
    fs = cfg.sample_rate

    csv_path = os.path.join(workspace, "tables",
                            "%ddb" % int(snr), "%s.csv" % data_type)
    with open(csv_path) as f:
        reader = csv.reader(f, delimiter='\t')
        data_csv = list(reader)

    for line in range(1, len(data_csv)):
        [speech_na, noise_na, n_onset, n_offset] = data_csv[line]
        n_onset = int(n_onset)
        n_offset = int(n_offset)
        # Читаем аудио поток речи
        speech_path = os.path.join(speech_dir, speech_na)
        (speech_audio, _) = sys.read_audio(speech_path, fs)

        # Читаем аудио поток шума
        noise_path = os.path.join(noise_dir, noise_na)
        (noise_audio, _) = sys.read_audio(noise_path, fs)
        noise_audio = noise_fit(speech_audio, noise_audio, n_onset, n_offset)

        # Масштабировать аудио поток речи по SNR
        scaler = get_amplitude_scaling_factor(speech_audio, noise_audio, snr)
        speech_audio *= scaler

        # Наложить шум на речь и нормализовать
        (speech_audio, noise_audio, mixed_audio,
         norm_factor) = superimpose_and_normalize(speech_audio, noise_audio)
        speech_audio = None
        noise_audio = None

        # Сохранить аугментированную запись
        mixed_na = os.path.join(
            "%s.%s" %
            (os.path.splitext(speech_na)[0], os.path.splitext(noise_na)[0]))
        mixed_path = os.path.join(workspace, "mixed_audio",
                                  data_type, "%ddb" % int(snr),
                                  "%s.wav" % mixed_na)
        sys.create_folder(os.path.dirname(mixed_path))
        sys.write_audio(mixed_path, mixed_audio, fs)
    print("Аугментация %s данных завершена.\t%s sec" %
          (data_type, (time.time() - timestamp, )))


def export_spectrograms(args):
    """Вычисляет для каждого зашумлённого аудио спектрограмму

    Args:
      workspace: str, путь к рабочему пространству
      speech_dir: str, путь к речевым данным
      data_type: str, тип данных для работы - ['train' | 'test']
      snr: float, отношение сигнал/шум

    """
    timestamp = time.time()
    workspace = args['workspace']
    speech_dir = args['speech_dir']
    data_type = args['data_type']
    snr = args['snr']

    fs = cfg.sample_rate
    frame_length = cfg.frameSize()
    frame_step = cfg.overlap()

    csv_path = os.path.join(workspace, "tables",
                            "%ddb" % int(snr), "%s.csv" % data_type)
    with open(csv_path) as f:
        reader = csv.reader(f, delimiter='\t')
        data_csv = list(reader)

    for line in range(1, len(data_csv)):
        [speech_na, noise_na, _, _] = data_csv[line]
        mixed_na = os.path.join(
            "%s.%s" %
            (os.path.splitext(speech_na)[0], os.path.splitext(noise_na)[0]))

        # Читаем чистую запись
        speech_path = os.path.join(speech_dir, speech_na)
        (clean_audio, _) = sys.read_audio(speech_path, fs)

        # Читаем зашумлённую запись
        mixed_path = os.path.join(workspace, "mixed_audio",
                                  data_type, "%ddb" % int(snr),
                                  "%s.wav" % mixed_na)
        (mixed_audio, _) = sys.read_audio(mixed_path, fs)

        # Получить спектры
        clean_spectrum = math.stft(clean_audio, frame_length, frame_step)
        mixed_spectrum = math.stft(mixed_audio, frame_length, frame_step)

        # Сохранить спектры на диск
        out_path = os.path.join(workspace, "spectrogram",
                                data_type, "%ddb" % int(snr),
                                "%s.p" % mixed_na)
        sys.create_folder(os.path.dirname(out_path))
        data = [clean_spectrum, mixed_spectrum]
        pickle.dump(data,
                    open(out_path, 'wb'),
                    protocol=pickle.HIGHEST_PROTOCOL)
    print("Экспорт %s спектрограм завершен.\t%s sec" %
          (data_type, (time.time() - timestamp, )))


def conver_to_tensor(args):
    """Преобразование всех спектральных признаков в единый тензор

    Args:
      workspace: str, путь к рабочему пространству
      data_type: str, тип данных для работы - ['train' | 'test']
      snr: float, отношение сигнал/шум
      shift: int, сдвиг
      n_combo: int, кол-во объединяемых спектров в контекст

    """
    timestamp = time.time()
    workspace = args['workspace']
    data_type = args['data_type']
    snr = args['snr']

    n_combo = args['n_combo']
    shift = args['shift']

    x_all = []  # [shape=(n_segs, n_combo, n_freq)]
    y_all = []  # [shape=(n_segs, n_freq)]

    spectr_dir = os.path.join(workspace, "spectrogram",
                              data_type, "%ddb" % int(snr))
    names = os.listdir(spectr_dir)
    for na in names:
        feat_path = os.path.join(spectr_dir, na)
        data = pickle.load(open(feat_path, 'rb'))
        [clean_spectrum, mixed_spectrum] = data
        data = None

        clean_spectrum = np.abs(clean_spectrum.T)
        mixed_spectrum = np.abs(mixed_spectrum.T)

        n_pad = int((n_combo - 1) / 2)
        clean_spectrum = util.pad_with_border(clean_spectrum, n_pad)
        mixed_spectrum = util.pad_with_border(mixed_spectrum, n_pad)

        clean_tensor = util.mat_2d_to_3d(clean_spectrum, n_combo, shift)
        clean_spectrum = None
        y_all.append(clean_tensor[:, n_pad, :])
        clean_tensor = None

        mixed_tensor = util.mat_2d_to_3d(mixed_spectrum, n_combo, shift)
        mixed_spectrum = None
        x_all.append(mixed_tensor)
        mixed_tensor = None

    x_all = np.concatenate(x_all, axis=0)
    y_all = np.concatenate(y_all, axis=0)

    x_all = math.safe_log(x_all)
    y_all = math.safe_log(y_all)

    out_path = os.path.join(workspace, "tensor", data_type,
                            "%ddb" % int(snr), "data.h5")
    sys.create_folder(os.path.dirname(out_path))
    with h5py.File(out_path, 'w') as hf:
        hf.create_dataset('x', data=x_all)
        hf.create_dataset('y', data=y_all)

    print("Преобразование %s данных в тензор завершено.\t%s sec" %
          (data_type, (time.time() - timestamp, )))


def compute_scaler(args):
    """Вычисление StandardScaler

    Args:
      workspace: str, путь к рабочему пространству
      data_type: str, тип данных для работы - ['train' | 'test']
      snr: float, отношение сигнал/шум

    """
    timestamp = time.time()
    workspace = args['workspace']
    data_type = args['data_type']
    snr = args['snr']

    hdf5_path = os.path.join(workspace, "tensor", data_type,
                             "%ddb" % int(snr), "data.h5")
    with h5py.File(hdf5_path, 'r') as hf:
        x = np.array(hf.get('x'))

    (n_segs, n_combo, n_freq) = x.shape
    x2d = x.reshape((n_segs * n_combo, n_freq))
    scaler = preprocessing.StandardScaler(with_mean=True,
                                          with_std=True).fit(x2d)

    out_path = os.path.join(workspace, "tensor", data_type,
                            "%ddb" % int(snr), "scaler.p")
    sys.create_folder(os.path.dirname(out_path))
    pickle.dump(scaler, open(out_path, 'wb'))

    print("Вычисление StandardScaler завершено.\t%s sec" %
          (time.time() - timestamp, ))

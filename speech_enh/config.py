# -*- coding: utf-8 -*-
sample_rate = 8000  # Частота дискретизации
frame_size_sec = 0.032  # Размер кадра = 32ms
frame_stride_sec = 0.016  # Шаг смещения кадра = 16ms
n_combo = 7  # Шаг смещения кадра = 16ms


def frameSize():
    """ Количество звуковых отсчётов в одном кадре.

    """
    return int(sample_rate * frame_size_sec)


def overlap():
    """ Количество звуковых отсчётов между
        началами следующих друг за другом кадров.

    """
    return int(sample_rate * frame_stride_sec)
